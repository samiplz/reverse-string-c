﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReverseString
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void getRev_Click(object sender, EventArgs e)
        {
            try
            {
                Char[] inputChar = txtReverse.Text.ToCharArray();

                Array.Reverse(inputChar);
                string finalTxt = new String(inputChar);
                lblReversed.Text = finalTxt;

            } catch(Exception nullString)
            {
                MessageBox.Show("Unknown Error Occured ");
            }






        }
    }
}
