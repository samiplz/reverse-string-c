﻿namespace ReverseString
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtReverse = new System.Windows.Forms.TextBox();
            this.getRev = new System.Windows.Forms.Button();
            this.lblReversed = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtReverse
            // 
            this.txtReverse.Location = new System.Drawing.Point(48, 50);
            this.txtReverse.Name = "txtReverse";
            this.txtReverse.Size = new System.Drawing.Size(189, 20);
            this.txtReverse.TabIndex = 0;
            // 
            // getRev
            // 
            this.getRev.Location = new System.Drawing.Point(98, 95);
            this.getRev.Name = "getRev";
            this.getRev.Size = new System.Drawing.Size(75, 23);
            this.getRev.TabIndex = 1;
            this.getRev.Text = "Reverse it !";
            this.getRev.UseVisualStyleBackColor = true;
            this.getRev.Click += new System.EventHandler(this.getRev_Click);
            // 
            // lblReversed
            // 
            this.lblReversed.AutoSize = true;
            this.lblReversed.Location = new System.Drawing.Point(45, 156);
            this.lblReversed.Name = "lblReversed";
            this.lblReversed.Size = new System.Drawing.Size(83, 13);
            this.lblReversed.TabIndex = 2;
            this.lblReversed.Text = "Reversed String";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lblReversed);
            this.Controls.Add(this.getRev);
            this.Controls.Add(this.txtReverse);
            this.Name = "Form1";
            this.Text = "Reverse String";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtReverse;
        private System.Windows.Forms.Button getRev;
        private System.Windows.Forms.Label lblReversed;
    }
}

